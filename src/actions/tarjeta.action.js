import React, { Component } from 'react'
import Swal from 'sweetalert2'
import TarjetaView from '../views/tarjeta.view'

class TarjetaAction extends Component {
  state = {
    costo: 0
  }

  /**
   * @author Brayan Salas
   * @description Comprueba el costo total del viaje
   * @memberof TarjetaAction
   */
  cobrar = () => {
    let pago = this.props.edad * 100
    
    this.setState({
      costo: pago
    }, () => {
      this.mostrarSwal()
    })
  }

  /**
   * @autor Brayan Salas
   * @description Mostrar correctamente el sweetalert
   * @memberof TarjetaAction
   */
  mostrarSwal = () => {
    Swal.fire({
      title: 'Listo',
      type: 'success',
      text: `Su costo total del viaje es de: ${this.state.costo}`,
      timer: 1500,
      showConfirmButton: false
    })
  }

  render () {
    return (
      <TarjetaView
      nombre={this.props.nombre}
      edad={this.props.edad}
      img={this.props.img}
      costo={this.state.costo} 
      cobrar={this.cobrar} />
    )
  }
}

export { TarjetaAction as default }
