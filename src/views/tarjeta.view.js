import React from 'react'
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import PropTypes from 'prop-types'

const TarjetaView = (props) => {
  return (
    <Card style={{ height: 'auto', width: '290px', marginTop: '40px', margin: 'auto', marginBottom: '40px', }}>
      <img style={{ borderRadius: '50%', marginTop: '20px' }} alt='asd' width='120px' height='120px' src={props.img} />
      <p>Bienvenido {props.nombre}</p>
      <Button style={{ backgroundColor: 'red', color: 'white', marginBottom: '10px' }} 
      onClick={props.cobrar}
      >Checar costo</Button>
    </Card>
  )
}

TarjetaView.propTypes = {
  nombre: PropTypes.string,
  costo: PropTypes.number,
  cobrar: PropTypes.func,
  refresh: PropTypes.func
}

export { TarjetaView as default }
