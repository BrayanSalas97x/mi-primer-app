import React, { Component } from 'react';
import TarjetaAction from './actions/tarjeta.action'
import './App.css';

let infoTarjetas = [
  {
    nombre: 'Brayan',
    edad: '21',
    img: 'https://scontent.fntr5-1.fna.fbcdn.net/v/t1.0-9/53021183_10205204101377500_7686780231817887744_n.jpg?_nc_cat=101&_nc_ht=scontent.fntr5-1.fna&oh=5f1d6aba7f381c284730225d87eeb7c4&oe=5D06D2E7'
  },
  {
    nombre: 'Julio',
    edad: '22',
    img: 'https://scontent.fntr5-1.fna.fbcdn.net/v/t1.0-9/26169751_1798809740129442_2043963156775930632_n.jpg?_nc_cat=100&_nc_ht=scontent.fntr5-1.fna&oh=febdddfdccd4900423dc418a61435770&oe=5D4E8809'
  },
  {
    nombre: 'Ivan',
    edad: '22',
    img: 'https://scontent.fntr5-1.fna.fbcdn.net/v/t1.0-9/46124012_1863659217037012_3843401121762639872_n.jpg?_nc_cat=106&_nc_ht=scontent.fntr5-1.fna&oh=e1b0d53361e957dd12314951b7f062d1&oe=5D404A04'
  },
  {
    nombre: 'Mark',
    edad: '22',
    img: 'https://scontent.fntr5-1.fna.fbcdn.net/v/t1.0-9/13533233_513476332173412_2822954927101821888_n.jpg?_nc_cat=105&_nc_ht=scontent.fntr5-1.fna&oh=b7e71c13fe713c5d78fe570d3a656a72&oe=5D39B271'
  }
]

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          {
            infoTarjetas.map((persona, key) => {
              return (
                <TarjetaAction nombre={persona.nombre} edad={persona.edad} img={persona.img} key={key} />
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default App;
